const mongoose = require('mongoose');
const Grade = require('./grade');
const uri = 'mongodb://localhost:27017/students';
mongoose.connect(uri);

const db = mongoose.connection;

db.on('error',()=>{
  console.log('no se pudo conectar');
});

db.on('open',()=>{
  console.log('conexion exitosa');
  // 2) El conjunto de datos contiene 4 calificaciones de n estudiantes. Confirma que se importo correctamente la colección con los siguientes comandos en la terminal de mongo:
  //
  // >use students;
  // >db.grades.count()
  //2) ¿Cuántos registros arrojo el comando count?"

  Grade.count({},(err, count)=> {
    if (err)  {
      console.log("no se pudo contar")
    }
    console.log("2) ¿Cuántos registros arrojo el comando count?")
    console.log(count); //800
  });

//3) Encuentra todas las calificaciones del estudiante con el id numero 4
  Grade.find({"student_id":4},(err,docs)=>{
    console.log("3) Encuentra todas las calificaciones del estudiante con el id numero 4")
    console.log(docs)
  //     [
  //   {
  //     _id: 50906d7fa3c412bb040eb587,
  //     student_id: 4,
  //     type: 'exam',
  //     score: 87.89071881934647
  //   },
  //   {
  //     _id: 50906d7fa3c412bb040eb588,
  //     student_id: 4,
  //     type: 'quiz',
  //     score: 27.29006335059361
  //   },
  //   {
  //     _id: 50906d7fa3c412bb040eb589,
  //     student_id: 4,
  //     type: 'homework',
  //     score: 5.244452510818443
  //   },
  //   {
  //     _id: 50906d7fa3c412bb040eb58a,
  //     student_id: 4,
  //     type: 'homework',
  //     score: 28.656451042441
  //   }
  // ]
  });

  // 4) ¿Cuántos registros hay de tipo exam?
  Grade.count({"type":"exam"},(err, count)=> {
    if (err)  {
      console.log("no se pudo contar")
    }
    console.log("4) ¿Cuántos registros hay de tipo exam?")
    console.log(count); //200
  });
  // 5) ¿Cuántos registros hay de tipo homework?
  Grade.count({"type":"homework"},(err, count)=> {
    if (err)  {
      console.log("no se pudo contar")
    }
    console.log("5) ¿Cuántos registros hay de tipo homework?")
    console.log(count); //400
  });
  // 6) ¿Cuántos registros hay de tipo quiz?
  Grade.count({"type":"quiz"},(err, count)=> {
    if (err)  {
      console.log("no se pudo contar")
    }
    console.log("6) ¿Cuántos registros hay de tipo quiz?")
    console.log(count); //200
  });

  // 7) Elimina todas las calificaciones del estudiante con el id numero 3
  Grade.deleteMany({"student_id":3},(err)=>{
    console.log("7) Elimina todas las calificaciones del estudiante con el id numero 3")
    if(err){
      console.log("no se pudo eliminar")
    }
  });

  // 8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?
  Grade.distinct('student_id',{"type":"homework","score":75.29561445722392},(err,results)=>{
    console.log("8) ¿Qué estudiantes obtuvieron 75.29561445722392 en una tarea ?")
    console.log(results)
  });
  // 9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100
  Grade.findByIdAndUpdate(mongoose.Types.ObjectId("50906d7fa3c412bb040eb591"),{"score":100},(err,doc)=>{
    console.log("9) Actualiza las calificaciones del registro con el uuid 50906d7fa3c412bb040eb591 por 100")
    if(err){
      console.log("no se pudo actualizar")
    } else {
      console.log(doc)
//    {
  //   _id: 50906d7fa3c412bb040eb591,
  //   student_id: 6,
  //   type: 'homework',
  //   score: 81.23822046161325
  //  }
    }
  })
  // 10) A qué estudiante pertenece esta calificación.
  Grade.findById(mongoose.Types.ObjectId("50906d7fa3c412bb040eb591"),'student_id',(err,docs)=>{
    console.log("10) A qué estudiante pertenece esta calificación.")
    if(err){
      console.log("no se encontro")
    } else {
      console.log(docs)
      // { _id: 50906d7fa3c412bb040eb591, student_id: 6 }
    }
  });
});
