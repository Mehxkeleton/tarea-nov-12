const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = Schema({
  _student_id:Number,
  _type:String,
  _score:Number
});

class Grade {
  constructor(type, score){
    this._student_id=student_id;
    this._type=type;
    this._score=score;
  }
  get type(){
    return this.student_id;
  }
  set type(v){
    this.student_id = v;
  }
  get type(){
    return this._type;
  }
  set type(v){
    this._type = v;
  }
  get score(){
    return this._score;
  }
  set score(v){
    this._score = v;
  }
}

schema.loadClass(Grade);
module.exports = mongoose.model('Grade', schema);
